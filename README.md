# whereamid - Find out in which network you are and execute custom up/down scripts

This script is my answer to the question on how to find out to which LAN network my laptop is connected.
It waits for changes in the interface state and detects the network it is in using provided modules.
Each module can define a way to detect the network one is in in a different way using different sets of arguments.
For each profile, one defines up and down functions, thus enabling setup of e.g. VLANs, VPN tunnels etc.

## Using whereamid

Use `whereamid.conf.sh.template` as a template to create your own whereamid config.
It utilizes bash syntax, so you can also insert your own conditional code.

The config provides the following values:

  * `INTERFACES=()` - List of all interfaces which should be scanned
  * `MODULES=()` - List of all enabled modules. Each module needs a corresponding module script in `$MODULES_DIR`
  * `PROFILES=()` - List of all enabled profiles. For each Profile, identifiers can be specified to decide when to start the profile.
  * `DEFAULT_PROFILE=""` - String of the default profile name.
  * `STATE_FILE` - File where to save the connection and profile state
  * `MODULES_DIR` - Directory where the module scripts are located
    * each module has a script named `mod_${module}.sh`
  * `PROFILES_DIR` - Directory where the profile scripts are located
    * each profile has a script named `$[profile}.sh`
  * `IDENTIFIER_${profile}` - Sets the identifier/detector for the profile `${profile}` and args passed to the profile detector

Once configured, start whereamid and set the `CONFIG` environment variable to the location of the configuration file.

## Writing modules

Each module has a file `mod_${module}.sh` file in the modules directory, where `$[module}` is the module's name.
A module can execute arbitrary code on load time, but normally, only functions shall be defined and variables should be defined or extended.

### Defining profile detectors

Profile detectors are functions to determine if a certain profile shall be activated.
For a profile detector to become available, append its name to the `PROFILE_DETECTORS` list:

```
PROFILE_DETECTORS+=( "MYDETECTORNAME" )
```

Afterwards, define a function `detect_profile_MYDETECTORNAME`.
It shall accept the following signature:

```
detect_profile_ra [INTERFACE] [DETECTOR] [ARGS...]
```
`ARGS...` is an arbitrary number of arguments which can be set in the configuration file.

The function must return `0` if the profile shall be activated and `1` if it shall not.

## Writing profiles

Each profile is a shell script defining the functions `profile_up` and `profile_down`.

One can execute arbitrary commands within these functions.
