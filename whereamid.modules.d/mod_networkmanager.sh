#!/bin/bash

# Usage: IDENTIFIER_...="networkmanager [Network name/UUID]..."
# Network name/UUID can be anything nmcli connection show can use to
# Obtain the right network which shall be detected. In the best case,
# the UUID of the network profile is used.

PROFILE_DETECTORS+=(
	"networkmanager"
)

detect_profile_networkmanager() {
	local iface="$1"
	shift 2

	debug_msg "[NetworkManager] Checking"

	for nm_network in $@; do
		debug_msg "[NetworkManager] Checking network $nm_network"
		local network_uuid=$(nmcli -t -c no connection show --active "$nm_network" | grep "^connection.uuid.*$" | awk 'BEGIN { FS=":"; } { print $2; }')
		if [[ "$?" != "0" ]]; then
			debug_msg "[NetworkManager] Can't determine UUID for $nm_network"
			continue
		fi

		debug_msg "[NetworkManager] UUID is $network_uuid"

		local network_iface=$(nmcli -t -c no connection show --active | grep "$network_uuid" | awk 'BEGIN { FS=":"; } { print $4; }')
		if [[ "$network_iface" != "$iface" ]]; then
			debug_msg "[NetworkManager] $network_iface is not $iface"
			continue
		fi

		return 0
	done
	return 1
}
