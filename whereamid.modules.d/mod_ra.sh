#!/bin/bash

# Usage: IDENTIFIER_...="ra [IPv6Addr] [IPv6Addr] [IPv6Addr...]"
# IPv6Addr shall be an IPv6 address which is the address of the default gateway.
# One can set an arbitrary number of IPv6 addresses

PROFILE_DETECTORS+=(
	"ra"
)

detect_profile_ra() {
	local iface="$1"
	shift 2

	local ra="$(ip --color=never -6 route show dev $iface proto ra default | awk '{print $3;}' | xargs)"

	for v6addr in $@; do
		for ra_addr in $ra; do
			if [[ "$v6addr" == "$ra_addr" ]]; then
				return 0
			fi
		done
	done

	return 1
}
